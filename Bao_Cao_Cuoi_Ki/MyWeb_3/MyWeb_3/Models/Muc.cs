﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyWeb_3.Models
{
    public class Muc
    {
        public int ID { set; get; }
        [Required(ErrorMessage = ("Không bỏ trống!"))]
        public String Noidung { set; get; }

        public virtual ICollection<Baiviet> Baiviets { set; get; }
    }
}