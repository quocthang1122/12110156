﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyWeb_3.Models
{
    public class Thongbao
    {
        public int ID { set; get; }
        [Required(ErrorMessage = ("Không bỏ trống!"))]
        public String TenTB { set; get; }
        [Required(ErrorMessage = ("Không bỏ trống!"))]
        public String LoaiTB { set; get; }
        [Required(ErrorMessage = ("Không bỏ trống!"))]
        public String Tinhtrang { set; get; }

        public virtual Binhluan Binhluan { set; get; }
        public virtual ICollection<UserProfile> UserProfiles { set; get; }
    }
}