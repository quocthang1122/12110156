﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyWeb_3.Models
{
    public class Chude
    {
        public int ID { set; get; }
        public String Nguoitao { set; get; }
        [Required(ErrorMessage = ("Không bỏ trống!"))]
        [StringLength(50, ErrorMessage = "So luong ki tu trong khoang 5-->50", MinimumLength = 5)]
        public String Tieude { set; get; }
        [Required(ErrorMessage = ("Không bỏ trống!"))]
        [StringLength(5000, ErrorMessage = "So luong ki tu trong khoang 10-->5000", MinimumLength = 10)]
        public String Noidung { set; get; }
        [Required(ErrorMessage = ("Chưa nhập ngày"))]
        [DataType(DataType.Date)]
        public DateTime Ngaytao { set; get; }
        [Required(ErrorMessage = ("Chưa nhập ngày"))]
        [DataType(DataType.Date)]
        public DateTime Ngaychinhsua { set; get; }
        public int Luotcamon { set; get; }
        public int UserProfileUserId { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public virtual ICollection<Binhluan> Binhluans { set; get; }
    }
}