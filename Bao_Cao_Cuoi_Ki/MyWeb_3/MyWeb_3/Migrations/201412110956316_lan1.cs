namespace MyWeb_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false),
                        Hoten = c.String(),
                        Gioitinh = c.String(),
                        Namsinh = c.DateTime(nullable: false),
                        Diachi = c.String(),
                        SoDT = c.String(),
                        Email = c.String(),
                        Chieucao = c.Single(nullable: false),
                        CanNang = c.Single(nullable: false),
                        Thoigian = c.String(),
                        ChisoBMI = c.Single(nullable: false),
                        KituGN = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Baiviets",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Chuyenmuc = c.String(),
                        Tieude = c.String(),
                        Noidung = c.String(),
                        MucID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .ForeignKey("dbo.Mucs", t => t.MucID, cascadeDelete: true)
                .Index(t => t.UserProfile_UserId)
                .Index(t => t.MucID);
            
            CreateTable(
                "dbo.Mucs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Noidung = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Chudes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nguoitao = c.String(),
                        Tieude = c.String(),
                        Noidung = c.String(),
                        Ngaytao = c.DateTime(nullable: false),
                        Ngaychinhsua = c.DateTime(nullable: false),
                        Luotcamon = c.Int(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.Binhluans",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NDBinhluan = c.String(),
                        Ngaytao = c.DateTime(nullable: false),
                        Ngaychinhsua = c.DateTime(nullable: false),
                        ChudeID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Chudes", t => t.ChudeID, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.ChudeID)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.Thongbaos",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenTB = c.String(),
                        LoaiTB = c.String(),
                        Tinhtrang = c.String(),
                        Binhluan_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Binhluans", t => t.Binhluan_ID)
                .Index(t => t.Binhluan_ID);
            
            CreateTable(
                "dbo.Tinhtoans",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ChisoBMI = c.Single(nullable: false),
                        Lopnguoi = c.String(),
                        PPhopli = c.String(),
                        ThucphamBS = c.String(),
                        LuyentapBS = c.String(),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.UserProfile_Thongbao",
                c => new
                    {
                        UserProfileUserID = c.Int(nullable: false),
                        ThongbaoID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserProfileUserID, t.ThongbaoID })
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserID, cascadeDelete: true)
                .ForeignKey("dbo.Thongbaos", t => t.ThongbaoID, cascadeDelete: true)
                .Index(t => t.UserProfileUserID)
                .Index(t => t.ThongbaoID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserProfile_Thongbao", new[] { "ThongbaoID" });
            DropIndex("dbo.UserProfile_Thongbao", new[] { "UserProfileUserID" });
            DropIndex("dbo.Tinhtoans", new[] { "UserProfile_UserId" });
            DropIndex("dbo.Thongbaos", new[] { "Binhluan_ID" });
            DropIndex("dbo.Binhluans", new[] { "UserProfile_UserId" });
            DropIndex("dbo.Binhluans", new[] { "ChudeID" });
            DropIndex("dbo.Chudes", new[] { "UserProfileUserId" });
            DropIndex("dbo.Baiviets", new[] { "MucID" });
            DropIndex("dbo.Baiviets", new[] { "UserProfile_UserId" });
            DropForeignKey("dbo.UserProfile_Thongbao", "ThongbaoID", "dbo.Thongbaos");
            DropForeignKey("dbo.UserProfile_Thongbao", "UserProfileUserID", "dbo.UserProfile");
            DropForeignKey("dbo.Tinhtoans", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.Thongbaos", "Binhluan_ID", "dbo.Binhluans");
            DropForeignKey("dbo.Binhluans", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.Binhluans", "ChudeID", "dbo.Chudes");
            DropForeignKey("dbo.Chudes", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.Baiviets", "MucID", "dbo.Mucs");
            DropForeignKey("dbo.Baiviets", "UserProfile_UserId", "dbo.UserProfile");
            DropTable("dbo.UserProfile_Thongbao");
            DropTable("dbo.Tinhtoans");
            DropTable("dbo.Thongbaos");
            DropTable("dbo.Binhluans");
            DropTable("dbo.Chudes");
            DropTable("dbo.Mucs");
            DropTable("dbo.Baiviets");
            DropTable("dbo.UserProfile");
        }
    }
}
