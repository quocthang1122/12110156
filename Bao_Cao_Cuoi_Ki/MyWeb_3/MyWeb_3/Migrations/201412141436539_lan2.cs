namespace MyWeb_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Baiviets", "Tieude", c => c.String(nullable: false));
            AlterColumn("dbo.Baiviets", "Noidung", c => c.String(nullable: false));
            AlterColumn("dbo.Mucs", "Noidung", c => c.String(nullable: false));
            AlterColumn("dbo.Chudes", "Tieude", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Chudes", "Noidung", c => c.String(nullable: false));
            AlterColumn("dbo.Binhluans", "NDBinhluan", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Thongbaos", "TenTB", c => c.String(nullable: false));
            AlterColumn("dbo.Thongbaos", "LoaiTB", c => c.String(nullable: false));
            AlterColumn("dbo.Thongbaos", "Tinhtrang", c => c.String(nullable: false));
            AlterColumn("dbo.Tinhtoans", "Lopnguoi", c => c.String(nullable: false));
            AlterColumn("dbo.Tinhtoans", "PPhopli", c => c.String(nullable: false));
            AlterColumn("dbo.Tinhtoans", "ThucphamBS", c => c.String(nullable: false));
            AlterColumn("dbo.Tinhtoans", "LuyentapBS", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tinhtoans", "LuyentapBS", c => c.String());
            AlterColumn("dbo.Tinhtoans", "ThucphamBS", c => c.String());
            AlterColumn("dbo.Tinhtoans", "PPhopli", c => c.String());
            AlterColumn("dbo.Tinhtoans", "Lopnguoi", c => c.String());
            AlterColumn("dbo.Thongbaos", "Tinhtrang", c => c.String());
            AlterColumn("dbo.Thongbaos", "LoaiTB", c => c.String());
            AlterColumn("dbo.Thongbaos", "TenTB", c => c.String());
            AlterColumn("dbo.Binhluans", "NDBinhluan", c => c.String());
            AlterColumn("dbo.Chudes", "Noidung", c => c.String());
            AlterColumn("dbo.Chudes", "Tieude", c => c.String());
            AlterColumn("dbo.Mucs", "Noidung", c => c.String());
            AlterColumn("dbo.Baiviets", "Noidung", c => c.String());
            AlterColumn("dbo.Baiviets", "Tieude", c => c.String());
        }
    }
}
