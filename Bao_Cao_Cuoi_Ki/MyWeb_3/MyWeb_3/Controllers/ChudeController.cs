﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyWeb_3.Models;

namespace MyWeb_3.Controllers
{
    [Authorize]
    public class ChudeController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Chude/

        public ActionResult Index()
        {
            var chudes = db.Chudes.Include(c => c.UserProfile);
            return View(chudes.ToList());
        }

        //
        // GET: /Chude/Details/5

        public ActionResult Details(int id = 0)
        {
            Chude chude = db.Chudes.Find(id);
            ViewData["idpost"] = id;
            if (chude == null)
            {
                return HttpNotFound();
            }
            return View(chude);
        }

        //
        // GET: /Chude/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Chude/Create

        [HttpPost]
        public ActionResult Create(Chude chude)
        {
            if (ModelState.IsValid)
            {
                chude.Ngaytao = DateTime.Now;
                chude.Ngaychinhsua = DateTime.Now;
                db.Chudes.Add(chude);
                db.SaveChanges();
                return RedirectToAction("Details/" + chude.ID, "Chude");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", chude.UserProfileUserId);
            return View(chude);
        }

        //
        // GET: /Chude/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Chude chude = db.Chudes.Find(id);
            if (chude == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", chude.UserProfileUserId);
            return View(chude);
        }

        //
        // POST: /Chude/Edit/5

        [HttpPost]
        public ActionResult Edit(Chude chude)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chude).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", chude.UserProfileUserId);
            return View(chude);
        }

        //
        // GET: /Chude/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Chude chude = db.Chudes.Find(id);
            if (chude == null)
            {
                return HttpNotFound();
            }
            return View(chude);
        }

        //
        // POST: /Chude/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Chude chude = db.Chudes.Find(id);
            db.Chudes.Remove(chude);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}