﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyWeb_3.Models;

namespace MyWeb_3.Controllers
{
    [Authorize]
    public class TinhtoanController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Tinhtoan/

        public ActionResult Index()
        {
            return View(db.Tinhtoans.ToList());
        }

        //
        // GET: /Tinhtoan/Details/5

        public ActionResult Details(int id = 0)
        {
            Tinhtoan tinhtoan = db.Tinhtoans.Find(id);
            if (tinhtoan == null)
            {
                return HttpNotFound();
            }
            return View(tinhtoan);
        }

        //
        // GET: /Tinhtoan/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Tinhtoan/Create

        [HttpPost]
        public ActionResult Create(Tinhtoan tinhtoan)
        {
            if (ModelState.IsValid)
            {
                db.Tinhtoans.Add(tinhtoan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tinhtoan);
        }

        //
        // GET: /Tinhtoan/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Tinhtoan tinhtoan = db.Tinhtoans.Find(id);
            if (tinhtoan == null)
            {
                return HttpNotFound();
            }
            return View(tinhtoan);
        }

        //
        // POST: /Tinhtoan/Edit/5

        [HttpPost]
        public ActionResult Edit(Tinhtoan tinhtoan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tinhtoan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tinhtoan);
        }

        //
        // GET: /Tinhtoan/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Tinhtoan tinhtoan = db.Tinhtoans.Find(id);
            if (tinhtoan == null)
            {
                return HttpNotFound();
            }
            return View(tinhtoan);
        }

        //
        // POST: /Tinhtoan/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Tinhtoan tinhtoan = db.Tinhtoans.Find(id);
            db.Tinhtoans.Remove(tinhtoan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}