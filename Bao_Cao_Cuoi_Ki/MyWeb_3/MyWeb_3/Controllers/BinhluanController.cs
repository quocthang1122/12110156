﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyWeb_3.Models;

namespace MyWeb_3.Controllers
{
    [Authorize]
    public class BinhluanController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Binhluan/

        public ActionResult Index()
        {
            var binhluans = db.Binhluans.Include(b => b.Chude);
            return View(binhluans.ToList());
        }

        //
        // GET: /Binhluan/Details/5

        public ActionResult Details(int id = 0)
        {
            Binhluan binhluan = db.Binhluans.Find(id);
            if (binhluan == null)
            {
                return HttpNotFound();
            }
            return View(binhluan);
        }

        //
        // GET: /Binhluan/Create

        public ActionResult Create()
        {
            ViewBag.ChudeID = new SelectList(db.Chudes, "ID", "Nguoitao");
            return View();
        }

        //
        // POST: /Binhluan/Create

        [HttpPost]
        public ActionResult Create(Binhluan binhluan, int idpost)
        {
            if (ModelState.IsValid)
            {
                
                binhluan.ChudeID = idpost;
                binhluan.Ngaytao = DateTime.Now;
                binhluan.Ngaychinhsua = DateTime.Now;
                db.Binhluans.Add(binhluan);
                db.SaveChanges();
                return RedirectToAction("Details/" + idpost, "Chude");
            }

            ViewBag.ChudeID = new SelectList(db.Chudes, "ID", "Nguoitao", binhluan.ChudeID);
            return View(binhluan);
        }

        //
        // GET: /Binhluan/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Binhluan binhluan = db.Binhluans.Find(id);
            if (binhluan == null)
            {
                return HttpNotFound();
            }
            ViewBag.ChudeID = new SelectList(db.Chudes, "ID", "Nguoitao", binhluan.ChudeID);
            return View(binhluan);
        }

        //
        // POST: /Binhluan/Edit/5

        [HttpPost]
        public ActionResult Edit(Binhluan binhluan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(binhluan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ChudeID = new SelectList(db.Chudes, "ID", "Nguoitao", binhluan.ChudeID);
            return View(binhluan);
        }

        //
        // GET: /Binhluan/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Binhluan binhluan = db.Binhluans.Find(id);
            if (binhluan == null)
            {
                return HttpNotFound();
            }
            return View(binhluan);
        }

        //
        // POST: /Binhluan/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Binhluan binhluan = db.Binhluans.Find(id);
            db.Binhluans.Remove(binhluan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}