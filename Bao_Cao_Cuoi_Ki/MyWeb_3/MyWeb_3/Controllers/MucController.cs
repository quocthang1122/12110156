﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyWeb_3.Models;

namespace MyWeb_3.Controllers
{
    [Authorize]
    public class MucController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Muc/

        public ActionResult Index()
        {
           
            return View(db.Mucs.ToList());
        }

        //
        // GET: /Muc/Details/5

        public ActionResult Details(int id = 0)
        {
            Muc muc = db.Mucs.Find(id);
            if (muc == null)
            {
                return HttpNotFound();
            }
            return View(muc);
        }

        //
        // GET: /Muc/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Muc/Create

        [HttpPost]
        public ActionResult Create(Muc muc)
        {
            if (ModelState.IsValid)
            {
                db.Mucs.Add(muc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(muc);
        }

        //
        // GET: /Muc/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Muc muc = db.Mucs.Find(id);
            if (muc == null)
            {
                return HttpNotFound();
            }
            return View(muc);
        }

        //
        // POST: /Muc/Edit/5

        [HttpPost]
        public ActionResult Edit(Muc muc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(muc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(muc);
        }

        //
        // GET: /Muc/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Muc muc = db.Mucs.Find(id);
            if (muc == null)
            {
                return HttpNotFound();
            }
            return View(muc);
        }

        //
        // POST: /Muc/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Muc muc = db.Mucs.Find(id);
            db.Mucs.Remove(muc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}