﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog6.Models
{
    [Table("Post")]
    public class Post
    {
        public int ID { set; get; }
        [Required(ErrorMessage = ("Khong bo trong"))]
        [StringLength(500, ErrorMessage = "So luong ki tu trong khoang 20-->250", MinimumLength = 20)]
        public String Title { set; get; }
        [Required(ErrorMessage = ("Khong bo trong"))]
        [StringLength(5000, ErrorMessage = "So luong ki tu toi thieu 50", MinimumLength = 50)]
        public String Body { set; get; }
        [Required(ErrorMessage=( "Chua nhap ngay"))]
        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }
        [Required(ErrorMessage=( "Chua nhap ngay"))]
        [DataType(DataType.Date)]
        public DateTime DateUpdated { set; get; }
        [Required(ErrorMessage=( "Khong bo trong"))]
        public int AccountID { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public virtual Account Account { set; get; }
    }
}