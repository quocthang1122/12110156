﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog6.Models
{
    [Table("Account")]
    public class Account
    {
        [Required(ErrorMessage=( "Khong bo trong"))]
        [StringLength(100, ErrorMessage = "So luong ki tu trong khoang 1-->100", MinimumLength = 1)]
        public String FirstName { set; get; }
        [Required(ErrorMessage = ("Khong bo trong"))]
        [StringLength(100, ErrorMessage = "So luong ki tu trong khoang 1-->100", MinimumLength = 1)]
        public String LastName { set; get; }
        [Required(ErrorMessage = ("Khong bo trong"))]
        [DataType(DataType.Password)]
        public String Password { set; get; }
        [Required(ErrorMessage = ("Khong bo trong"))]
        [DataType(DataType.EmailAddress,ErrorMessage="Dia chi email sai!")]
        public String Email { set; get; }
        public int AccountID { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}