﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog8.Models
{
    [Table("Comment")]
    public class Comment
    {
        public int ID { set; get; }
        [Required(ErrorMessage = ("Khong bo trong"))]
        [StringLength(5000, ErrorMessage = "So luong ki tu toi thieu 50", MinimumLength = 50)]
        public String Body { set; get; }
        [Required(ErrorMessage = ("Chua nhap ngay"))]
        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }
        [Required(ErrorMessage = ("Chua nhap ngay"))]
        [DataType(DataType.Date)]
        public DateTime DateUpdated { set; get; }
        [Required(ErrorMessage = ("Khong bo trong"))]
        public String Author { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}