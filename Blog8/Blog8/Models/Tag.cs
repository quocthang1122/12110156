﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog8.Models
{
    [Table("Tag")]
    public class Tag
    {
        public int TagID { set; get; }
        [StringLength(100, ErrorMessage = "So luong ki tu trong khoang 10-->100", MinimumLength = 10)]
        [Required(ErrorMessage = ("Khong bo trong"))]
        public String Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}