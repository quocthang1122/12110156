namespace Blog8.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Post", "UserProfile_UserId", "dbo.UserProfile");
            DropIndex("dbo.Post", new[] { "UserProfile_UserId" });
            RenameColumn(table: "dbo.Post", name: "UserProfile_UserId", newName: "UserProfileUserId");
            AddForeignKey("dbo.Post", "UserProfileUserId", "dbo.UserProfile", "UserId", cascadeDelete: true);
            CreateIndex("dbo.Post", "UserProfileUserId");
            DropColumn("dbo.Post", "UserProfileId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Post", "UserProfileId", c => c.Int(nullable: false));
            DropIndex("dbo.Post", new[] { "UserProfileUserId" });
            DropForeignKey("dbo.Post", "UserProfileUserId", "dbo.UserProfile");
            RenameColumn(table: "dbo.Post", name: "UserProfileUserId", newName: "UserProfile_UserId");
            CreateIndex("dbo.Post", "UserProfile_UserId");
            AddForeignKey("dbo.Post", "UserProfile_UserId", "dbo.UserProfile", "UserId");
        }
    }
}
