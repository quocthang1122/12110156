﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyWeb_3.Models;

namespace MyWeb_3.Controllers
{
    [Authorize]
    public class ThongbaoController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Thongbao/

        public ActionResult Index()
        {
            return View(db.Thongbaos.ToList());
        }

        //
        // GET: /Thongbao/Details/5

        public ActionResult Details(int id = 0)
        {
            Thongbao thongbao = db.Thongbaos.Find(id);
            if (thongbao == null)
            {
                return HttpNotFound();
            }
            return View(thongbao);
        }

        //
        // GET: /Thongbao/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Thongbao/Create

        [HttpPost]
        public ActionResult Create(Thongbao thongbao)
        {
            if (ModelState.IsValid)
            {
                db.Thongbaos.Add(thongbao);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(thongbao);
        }

        //
        // GET: /Thongbao/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Thongbao thongbao = db.Thongbaos.Find(id);
            if (thongbao == null)
            {
                return HttpNotFound();
            }
            return View(thongbao);
        }

        //
        // POST: /Thongbao/Edit/5

        [HttpPost]
        public ActionResult Edit(Thongbao thongbao)
        {
            if (ModelState.IsValid)
            {
                db.Entry(thongbao).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(thongbao);
        }

        //
        // GET: /Thongbao/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Thongbao thongbao = db.Thongbaos.Find(id);
            if (thongbao == null)
            {
                return HttpNotFound();
            }
            return View(thongbao);
        }

        //
        // POST: /Thongbao/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Thongbao thongbao = db.Thongbaos.Find(id);
            db.Thongbaos.Remove(thongbao);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}