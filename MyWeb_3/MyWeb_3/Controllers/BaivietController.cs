﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyWeb_3.Models;

namespace MyWeb_3.Controllers
{
    [Authorize]
    public class BaivietController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Baiviet/

        public ActionResult Index()
        {
            var baiviets = db.Baiviets.Include(b => b.Muc);
            return View(baiviets.ToList());
        }

        //
        // GET: /Baiviet/Details/5

        public ActionResult Details(int id = 0)
        {
            Baiviet baiviet = db.Baiviets.Find(id);
            if (baiviet == null)
            {
                return HttpNotFound();
            }
            return View(baiviet);
        }

        //
        // GET: /Baiviet/Create

        public ActionResult Create()
        {
            ViewBag.MucID = new SelectList(db.Mucs, "ID", "Noidung");
            return View();
        }

        //
        // POST: /Baiviet/Create

        [HttpPost]
        public ActionResult Create(Baiviet baiviet)
        {
            if (ModelState.IsValid)
            {
                db.Baiviets.Add(baiviet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MucID = new SelectList(db.Mucs, "ID", "Noidung", baiviet.MucID);
            return View(baiviet);
        }

        //
        // GET: /Baiviet/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Baiviet baiviet = db.Baiviets.Find(id);
            if (baiviet == null)
            {
                return HttpNotFound();
            }
            ViewBag.MucID = new SelectList(db.Mucs, "ID", "Noidung", baiviet.MucID);
            return View(baiviet);
        }

        //
        // POST: /Baiviet/Edit/5

        [HttpPost]
        public ActionResult Edit(Baiviet baiviet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(baiviet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MucID = new SelectList(db.Mucs, "ID", "Noidung", baiviet.MucID);
            return View(baiviet);
        }

        //
        // GET: /Baiviet/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Baiviet baiviet = db.Baiviets.Find(id);
            if (baiviet == null)
            {
                return HttpNotFound();
            }
            return View(baiviet);
        }

        //
        // POST: /Baiviet/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Baiviet baiviet = db.Baiviets.Find(id);
            db.Baiviets.Remove(baiviet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}