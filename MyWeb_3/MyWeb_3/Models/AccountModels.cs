﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace MyWeb_3.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Baiviet> Baiviets { set; get; }
        public DbSet<Chude> Chudes { set; get; }
        public DbSet<Binhluan> Binhluans { set; get; }
        public DbSet<Thongbao> Thongbaos { set; get; }
        public DbSet<Tinhtoan> Tinhtoans { set; get; }
        public DbSet<Muc> Mucs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<UserProfile>()
                .HasMany(d => d.Thongbaos).WithMany(p => p.UserProfiles)
                .Map(t => t.MapLeftKey("UserProfileUserID").MapRightKey("ThongbaoID").ToTable("UserProfile_Thongbao"));
        }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        [Required(ErrorMessage = ("Không bỏ trống!"))]
        public string UserName { get; set; }

        public String Hoten { set; get; }

        public String Gioitinh { set; get; }

        public DateTime Namsinh { set; get; }

        public String Diachi { set; get; }

        public String SoDT { set; get; }

        public String Email { set; get; }

        public float Chieucao { set; get; }

        public float CanNang { set; get; }

        public String Thoigian { set; get; }
        public float ChisoBMI { set; get; }

        public String KituGN { set; get; }

        public virtual ICollection<Baiviet> Baiviets { set; get; }
        public virtual ICollection<Chude> Chudes { set; get; }
        public virtual ICollection<Thongbao> Thongbaos { set; get; }
        public virtual ICollection<Binhluan> Binhluans { set; get; }
        public virtual ICollection<Tinhtoan> Tinhtoans { set; get; }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
    public class Thongtin
    {
        public String Hoten { set; get; }

        public String Gioitinh { set; get; }

        [DataType(DataType.Date)]
        public DateTime Namsinh { set; get; }
        public String Diachi { set; get; }

        public String SoDT { set; get; }

        public float Chieucao { set; get; }

        public float CanNang { set; get; }

        public String Thoigian { set; get; }

        public float ChisoBMI { set; get; }
    }
    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required(ErrorMessage = ("Khong bo trong"))]
        [DataType(DataType.EmailAddress, ErrorMessage = "Dia chi email sai!")]
        public String Email { set; get; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = ("Không bỏ trống!"))]
        public String KituGN { set; get; }

        public String Hoten { set; get; }
        
        public String Gioitinh { set; get; }
        
        [DataType(DataType.Date)]
        public DateTime Namsinh { set; get; }
        public String Diachi { set; get; }
        
        public String SoDT { set; get; }
        
        public float Chieucao { set; get; }
        
        public float CanNang { set; get; }
        
        public String Thoigian { set; get; }
        public float ChisoBMI { set; get; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
