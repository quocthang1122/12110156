﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyWeb_3.Models
{
    public class Tinhtoan
    {
        public int ID { set; get; }
        [Required(ErrorMessage = ("Không bỏ trống!"))]
        public float ChisoBMI { set; get; }
        [Required(ErrorMessage = ("Không bỏ trống!"))]
        public String Lopnguoi { set; get; }
        [Required(ErrorMessage = ("Không bỏ trống!"))]
        public String PPhopli { set; get; }
        [Required(ErrorMessage = ("Không bỏ trống!"))]
        public String ThucphamBS { set; get; }
        [Required(ErrorMessage = ("Không bỏ trống!"))]
        public String LuyentapBS { set; get; }

        public virtual UserProfile UserProfile { set; get; }
    }
}