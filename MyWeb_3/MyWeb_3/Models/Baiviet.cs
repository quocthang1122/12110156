﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyWeb_3.Models
{
    public class Baiviet
    {
        public int ID { set; get; }
        public String Chuyenmuc { set; get; }
        [Required(ErrorMessage = ("Không bỏ trống!"))]
        public String Tieude { set; get; }
        [Required(ErrorMessage = ("Không bỏ trống!"))]
        public String Noidung { set; get; }
        public int MucID { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public virtual Muc Muc { set; get; }
    }
}