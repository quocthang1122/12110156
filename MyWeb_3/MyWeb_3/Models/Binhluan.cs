﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyWeb_3.Models
{
    public class Binhluan
    {
        public int ID { set; get; }
        [Required(ErrorMessage = ("Không bỏ trống!"))]
        [StringLength(500, ErrorMessage = "So luong ki tu trong khoang 10-->500", MinimumLength = 10)]
        public String NDBinhluan { set; get; }
        [Required(ErrorMessage = ("Chưa nhập ngày!"))]
        [DataType(DataType.Date)]
        public DateTime Ngaytao { set; get; }
        [Required(ErrorMessage = ("Chưa nhập ngày!"))]
        [DataType(DataType.Date)]
        public DateTime Ngaychinhsua { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - Ngaytao).Minutes;
            }
        }
        public int ChudeID { set; get; }

        public virtual Chude Chude { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public virtual ICollection<Thongbao> Thongbaos { set; get; }
    }
}